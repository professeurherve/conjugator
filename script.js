// Configuration des terminaisons
const terminaisons = {
    present: {
        je: 'e',
        tu: 'es',
        il: 'e',
        elle: 'e',
        on: 'e',
        nous: 'ons',
        vous: 'ez',
        ils: 'ent',
        elles: 'ent'
    },
    imparfait: {
        je: 'ais',
        tu: 'ais',
        il: 'ait',
        elle: 'ait',
        on: 'ait',
        nous: 'ions',
        vous: 'iez',
        ils: 'aient',
        elles: 'aient'
    },
    futur: {
        je: 'ai',
        tu: 'as',
        il: 'a',
        elle: 'a',
        on: 'a',
        nous: 'ons',
        vous: 'ez',
        ils: 'ont',
        elles: 'ont'
    }
};

// Mapping des radicaux vers leurs infinitifs
const radicaux = {
    'dans': {
        radical: 'dans-',
        infinitif: 'danser'
    },
    'jou': {
        radical: 'jou-',
        infinitif: 'jouer'
    },
    'cri': {
        radical: 'cri-',
        infinitif: 'crier'
    },
    'saut': {
        radical: 'saut-',
        infinitif: 'sauter'
    },
    'arriv': {
        radical: 'arriv-',
        infinitif: 'arriver'
    },
    'aim': {
        radical: 'aim-',
        infinitif: 'aimer'
    },
    'donn': {
        radical: 'donn-',
        infinitif: 'donner'
    }
};

function updateRadicalOptions() {
    const temps = document.getElementById("temps").value;
    const radicalSelect = document.getElementById("radical");
    const currentValue = radicalSelect.value;
    
    radicalSelect.innerHTML = '';
    
    Object.entries(radicaux).forEach(([value, {radical, infinitif}]) => {
        const option = document.createElement('option');
        option.value = value;
        option.textContent = temps === "futur" ? infinitif : radical;
        radicalSelect.appendChild(option);
    });
    
    radicalSelect.value = currentValue;
}

function updateConjugaison() {
    const pronom = document.getElementById("pronom").value;
    const radical = document.getElementById("radical").value;
    const temps = document.getElementById("temps").value;

    // Récupération et affichage de la terminaison
    const terminaison = terminaisons[temps][pronom];
    const terminaisonDisplay = document.getElementById("terminaisonDisplay");
    if (terminaisonDisplay) {
        terminaisonDisplay.textContent = terminaison;
    }

    // Construction de la phrase complète
    let verbeConjugue;
    if (temps === "futur") {
        // Pour le futur, on garde l'infinitif complet et on ajoute la terminaison
        const infinitif = radicaux[radical].infinitif;
        verbeConjugue = infinitif + terminaison;
    } else {
        verbeConjugue = radical + terminaison;
    }

    // Gestion des cas spéciaux (je + voyelle)
    const estJeAvecVoyelle = pronom === "je" && ['aim', 'arriv'].includes(radical);
    let phraseComplete = estJeAvecVoyelle 
        ? `J'${verbeConjugue}`
        : `${pronom.charAt(0).toUpperCase() + pronom.slice(1)} ${verbeConjugue}`;

    // Mise à jour de l'affichage final
    document.getElementById("resultat").textContent = phraseComplete;
}

// Écouteurs d'événements
document.querySelectorAll('select').forEach(select => {
    select.addEventListener('change', () => {
        if (select.id === "temps") {
            updateRadicalOptions();
        }
        updateConjugaison();
    });
});

// Initialisation
document.addEventListener('DOMContentLoaded', () => {
    updateRadicalOptions();
    updateConjugaison();
});