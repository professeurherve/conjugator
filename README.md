# Conjugator
C'est une application créée et maintenue par Hervé ALLESANT, ERUN à Marseille.

## A quoi ça sert ?
Conjugator permet de manipuler les bases verbales et les marques des temps simples en autonomie.
## Licence CC-by_nc-sa 4.0
La licence CC-by-nc-sa 4.0 permet toute exploitation de l’œuvre (partager, copier, reproduire, distribuer, communiquer, réutiliser, adapter) par tous moyens, sous tous formats. Toutes les exploitations de l’œuvre ou des œuvres dérivées, sauf à des fins commerciales, sont possibles.
Les obligations liées à la licence sont de :
• créditer les créateurs de la paternité des œuvres originales, d’en indiquer les sources et d’indiquer si des modifications ont été effectuées aux œuvres (obligation d’attribution) ;
• ne pas tirer profit (gain direct ou plus-value commerciale) de l’œuvre ou des œuvres dérivées ;
• diffuser les nouvelles créations selon des conditions identiques (selon la même licence) à celles de l’œuvre originale (donc autoriser à nouveau les modifications et interdire les utilisations commerciales).